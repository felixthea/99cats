CatsProject::Application.routes.draw do

  root to: "cats#index"
  resources :cats
  resources :cat_rental_requests do
    member do
      post 'approve', 'deny'
    end
  end

  resources :users
  resource :session
end
