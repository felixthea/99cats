class CatRentalRequestsController < ApplicationController

  before_filter :require_correct_user!, only: [:approve, :deny]

  def new
    @cat_rental_request = CatRentalRequest.new
    @cats = Cat.all
  end

  def create
    CatRentalRequest.create(params[:cat_rental_request])
    @cats = Cat.all
    redirect_to @cats
  end

  def approve
    @current_cat_rental = CatRentalRequest.find(params[:id])
    @current_cat_rental.approve!
    current_cat = Cat.find(@current_cat_rental.cat_id)
    redirect_to current_cat
  end

  def deny
    @current_cat_rental = CatRentalRequest.find(params[:cat[:id]])
    @current_cat_rental.deny!
    current_cat = Cat.find(@current_cat_rental.cat_id)
    redirect_to current_cat
  end

end
