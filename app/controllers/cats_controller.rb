class CatsController < ApplicationController

  before_filter :require_correct_user!, only: [:edit, :new]

  def index
    @cats = Cat.all
  end

  def show
    @cat = Cat.find(params[:id])
    @cat_rental_requests = CatRentalRequest.find_all_by_cat_id(params[:id])
    @cat_rental_requests.sort! { |a,b| a.start_date <=> b.start_date } unless @cat_rental_requests.nil?
  end

  def new
    @cat = Cat.new
  end

  def create
    params[:cat][:user_id] = current_user.id
    @cat = Cat.new(params[:cat])
    if @cat.save
      redirect_to cat_url(@cat.id)
    else
      flash[:notice] ||= []
      flash[:notice] << "Cat couldn't be created!"
      redirect_to new_cat_url
    end
  end

  def edit
    @cat = Cat.find(params[:id])
    render :edit
  end

end
