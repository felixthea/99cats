class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by_credentials(
      params[:user][:user_name],
      params[:user][:password]
    )

    if @user.nil?
      flash[:notice] ||= []
      flash[:notice] << "Credentials were incorrect!"
      redirect_to new_session_url
    else
      login_user!
      redirect_to cats_url
    end
  end

  def destroy
    logout_current_user!
    redirect_to new_session_url
  end
end
