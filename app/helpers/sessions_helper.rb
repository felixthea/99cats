module SessionsHelper

  def current_user
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def logout_current_user!
    current_user.reset_session_token!
    session[:session_token] = nil
  end

  def login_user!
    @user.session_token = User.generate_session_token
    @user.save!
    session[:session_token] = @user.session_token
  end

  def require_correct_user!
    if self.class == CatRentalRequestsController
      if current_user != CatRentalRequest.find(params[:id]).cat.owner
        flash[:notice] ||= []
        flash[:notice] << "You need to be the owner."
        redirect_to new_session_url
      end
    elsif self.class == CatsController
      if current_user != Cat.find(params[:id]).owner
        flash[:notice] ||= []
        flash[:notice] << "You need to be the owner."
        redirect_to new_session_url
      end
    end
  end
end


