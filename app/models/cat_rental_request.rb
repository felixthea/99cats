class CatRentalRequest < ActiveRecord::Base
  attr_accessible :cat_id, :start_date, :end_date, :status

  validates :cat_id, :start_date, :end_date, :status, presence: true
  validates :status, inclusion: { in: %w(PENDING APPROVED DENIED) }
  validate :no_overlapping_approvals
  before_validation :init_status

  belongs_to(
  :cat,
  class_name: "Cat",
  foreign_key: :cat_id,
  primary_key: :id

  )

  def no_overlapping_approvals
    if status == "APPROVED" && overlapping_approved_requests.count > 0
      errors.add(:base, "request overlaps with an approved request")
    end
  end

  def overlapping_requests
    existing_requests = CatRentalRequest.find_all_by_cat_id(cat_id)
    overlaps = []
    existing_requests.each do |existing_request|
      next if existing_request.id == id
      if (existing_request.start_date..existing_request.end_date).include?(self.start_date) ||
        (existing_request.start_date..existing_request.end_date).include?(self.end_date)
        overlaps << existing_request
      end
    end

    overlaps
  end

  def overlapping_pending_requests
    overlaps_with_approved = []

    overlapping_requests.each do |overlapping_request|
      if overlapping_request.status == "PENDING"
        overlaps_with_approved << overlapping_request
      end
    end

    overlaps_with_approved
  end

  def overlapping_approved_requests
    overlaps_with_approved = []

    overlapping_requests.each do |overlapping_request|
      if overlapping_request.status == "APPROVED"
        overlaps_with_approved << overlapping_request
      end
    end

    overlaps_with_approved
  end

  def approve!
    ActiveRecord::Base.transaction do
      self.update_attributes(status: "APPROVED")
      self.save!
      overlapping_pending_requests.each do |overlapping_request|
        overlapping_request.deny!
      end
    end
  end

  def deny!
    self.update_attributes(status: "DENIED")
    self.save!
  end



  protected

  def init_status
    @status ||= "PENDING"
  end
end
