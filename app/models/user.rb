require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :password_digest, :session_token, :user_name, :password

  validates :password_digest, :session_token, :user_name, presence: true
  validates :session_token, uniqueness: true

  before_validation :ensure_session_token

  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save!
  end

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.find_by_credentials(user_name, password)
    user = User.find_by_user_name(user_name)
    user.is_password?(password) ? user : nil
  end

  has_many(
    :cats,
    class_name: "Cat",
    foreign_key: :user_id,
    primary_key: :id,
    dependent: :destroy
  )

  private
  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end

end
