class Cat < ActiveRecord::Base
  attr_accessible :age, :birth_date, :color, :name, :sex, :user_id

  validates :age, :birth_date, :color, :name, :sex, :user_id, presence: true
  validates :age, numericality: {only_integer: true}
  validates :sex, inclusion: { in: %w(M F)}
  validates :color, inclusion: { in: %(brown black white red blue yellow) }

  has_many(
    :cat_rental_requests,
    class_name: "CatRentalRequest",
    foreign_key: :cat_id,
    primary_key: :id,
    dependent: :destroy
  )

  belongs_to(
  :owner,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id
  )

end
